#!/bin/bash

usage()
{
    echo "usage: $(basename $0) [-j make_thread]"
}

MAKE_THEARD=1
while getopts "j:" arg
do
	case $arg in
		 j)
			MAKE_THEARD=$OPTARG
			;;
		 ?) #当有不认识的选项的时候arg为?
			usage
			exit 1
			;;
	esac
done

pushd u-boot/
make rk3128_defconfig
make -j $MAKE_THEARD
popd

pushd kernel/ 
make fireprime_defconfig
make rk3128-fireprime.img -j $MAKE_THEARD
popd

source  build.sh 
make installclean
make -j $MAKE_THEARD
./mkimage.sh

echo "FirePrime build Android finish!"
